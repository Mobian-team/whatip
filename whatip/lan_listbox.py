from gi.repository import GObject
from gettext import gettext as _
from whatip.ping import NetScanner
from whatip.lan_listbox_row import LanListboxRow
from whatip.listbox_with_empty_state import ListBoxWithEmptyState


class LanListbox(ListBoxWithEmptyState):
    __gsignals__ = {
        'refresh_finished': (
            GObject.SignalFlags.RUN_LAST,
            None,
            (str,)
        )
    }

    def __init__(self, *args, **kwargs):
        super().__init__(
            'content-loading-symbolic',
            _('Scanning LAN...'),
            *args, **kwargs
        )
        # self.progressbar from parent class
        self.revealer = self.progress_revealer
        self.progress_sig_id, self.finished_sig_id = None, None

    def new_scanner(self, address, subnet_mask):
        self.scanner = NetScanner(address, subnet_mask)
        self.progress_sig_id = self.scanner.pool.connect(
            'pool_progress',
            self.on_progress
        )
        self.finished_sig_id = self.scanner.pool.connect(
            'pool_finished',
            self.on_finished
        )

    def refresh(self, target_l):
        self.set_show_empty_state(True)
        for target in target_l:
            self.revealer.set_reveal_child(True)
            self.new_scanner(*target)
            self.progressbar.set_fraction(0.0)
            self.scanner.scan()

    def on_progress(self, caller, progress, total):
        self.progressbar.set_fraction(progress/total)

    def on_finished(self, *args):
        self.revealer.set_reveal_child(False)
        self.empty()
        for device in self.scanner.devices:
            self.append(
                LanListboxRow(
                    device['address'],
                    device['hostname'],
                    device['ports']
                )
            )
        self.show()
        self.emit('refresh_finished', '')
