from gi.repository import Gtk


class ListBoxWithEmptyState(Gtk.ScrolledWindow):
    def __init__(self, icon_name, label):
        super().__init__(hscrollbar_policy=Gtk.PolicyType.NEVER)
        self.builder = Gtk.Builder.new_from_resource(
            '/org/gabmus/whatip/ui/listbox_with_empty_state.ui'
        )
        self.stack = self.builder.get_object('stack')
        self.overlay = self.builder.get_object('overlay')
        self.listbox = self.builder.get_object('listbox')
        self.clamp = self.builder.get_object('clamp')
        self.progress_revealer = self.builder.get_object('progress_revealer')
        self.progressbar = self.builder.get_object('progressbar')
        self.empty_state = self.builder.get_object('empty_state')
        self.set_child(self.stack)

        self.set_sort_func = self.listbox.set_sort_func
        self.get_row_at_index = self.listbox.get_row_at_index

        self.empty_state.set_icon_name(icon_name)
        self.empty_state.set_title(label)

    def set_show_empty_state(self, state: bool):
        self.stack.set_visible_child(self.overlay if state else self.clamp)

    def empty(self):
        self.set_show_empty_state(True)
        while True:
            row = self.listbox.get_row_at_index(0)
            if row:
                self.listbox.remove(row)
            else:
                break

    def append(self, *args, **kwargs):
        self.listbox.append(*args, **kwargs)
        self.set_show_empty_state(False)

    def remove(self, *args, **kwargs):
        self.listbox.remove(*args, **kwargs)
        if not self.listbox.get_row_at_index(0):
            self.set_show_empty_state(True)
