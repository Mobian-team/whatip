from gettext import gettext as _
from gi.repository import Adw, Gtk
from whatip.base_preferences import (
     MPreferencesPage, MPreferencesGroup, PreferencesToggleRow
)
from typing import Optional


class GeneralPreferencesPage(MPreferencesPage):
    def __init__(self):
        super().__init__(
            title=_('General'), icon_name='preferences-other-symbolic',
            pref_groups=[
                MPreferencesGroup(
                    title=_('General preferences'), rows=[
                        PreferencesToggleRow(
                            title=_('Enable third party services'),
                            conf_key='enable_third_parties'
                        ),
                        PreferencesToggleRow(
                            title=_('Dark mode'),
                            conf_key='dark_mode',
                            signal='dark_mode_changed'
                        )
                    ]
                )
            ]
        )


class PreferencesWindow(Adw.PreferencesWindow):
    def __init__(self, parent_win: Optional[Gtk.Window]):
        super().__init__(default_width=360, default_height=600)
        if parent_win:
            self.set_transient_for(parent_win)
            self.set_modal(True)
        self.pages = [
            GeneralPreferencesPage(),
        ]
        for p in self.pages:
            self.add(p)
