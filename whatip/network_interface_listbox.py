from gi.repository import GLib
from gettext import gettext as _
from whatip.network_interface import (
    NetworkInterface,
    get_all_interfaces,
    get_internet_interface
)
from whatip.network_interface_listbox_row import NetworkInterfaceListboxRow
from whatip.listbox_with_empty_state import ListBoxWithEmptyState
from whatip.confManager import ConfManager
from threading import Thread


class NetworkInterfaceListbox(ListBoxWithEmptyState):
    def __init__(self, *args, **kwargs):
        super().__init__(
            'network-wired-offline-symbolic',
            _('No network interfaces connected'),
            *args, **kwargs
        )
        self.confman = ConfManager()
        self.set_sort_func(self.sort_func, None, False)
        self.iface_l = list()
        # this happens in headerbar, called from app_window on startup
        # self.refresh(sync=True)

    def sort_func(self, row1, row2, data, notify_destroy):
        if row1.net_iface.interface_type == NetworkInterface.INTERNET:
            return False
        if row2.net_iface.interface_type == NetworkInterface.INTERNET:
            return True
        if (  # this below is a xor
                (
                    row1.net_iface.interface_type in
                    NetworkInterface.CATEGORY_REAL
                )
                ^
                (
                    row2.net_iface.interface_type in
                    NetworkInterface.CATEGORY_REAL
                )
        ):
            return (
                row1.net_iface.interface_type not in
                NetworkInterface.CATEGORY_REAL
            )
        return row1.net_iface.interface > row2.net_iface.interface

    def refresh(self, sync=False):
        t = Thread(
            target=self._refresh_async,
            daemon=True
        )
        t.start()
        if sync:
            t.join()

    def _refresh_async(self):
        self.iface_l = get_all_interfaces()
        GLib.idle_add(
            self._refresh_callback,
            self.iface_l
        )
        if self.confman.conf['enable_third_parties']:
            internet_iface = get_internet_interface()
            internet_iface is not None and GLib.idle_add(
                self.append,
                NetworkInterfaceListboxRow(internet_iface)
            )

    def _refresh_callback(self, iface_l):
        self.empty()
        for iface in iface_l:
            self.append(
                NetworkInterfaceListboxRow(iface)
            )
        else:
            self.show()
