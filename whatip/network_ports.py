from subprocess import check_output


def get_ports():
    ports = dict()
    out = check_output(['ss', '-tulnH']).decode()
    for line in out.split('\n'):
        if not line:
            continue
        port_type = line.split()[0].upper()
        port = line.split()[4].split(':')[-1]
        address = line[:line.index(port)-1].split()[-1]
        if port in ports.keys():
            ports[port]['addresses'].add(address)
            if ports[port]['type'] != port_type:
                ports[port]['type'] = 'TCP/UDP'
        else:
            ports[port] = {
                'addresses': {address},
                'type': port_type
            }
    for port in ports.keys():
        ports[port]['addresses'] = sorted(
            list(ports[port]['addresses'])
        )
    return ports
