import threading
from gi.repository import GLib, GObject


class ThreadPool(GObject.Object):
    __gsignals__ = {
        'pool_finished': (
            GObject.SignalFlags.RUN_LAST,
            None,
            (str,)
        ),
        'pool_progress': (
            GObject.SignalFlags.RUN_FIRST,
            None,
            (int, int)  # done, total
        )
    }

    def __init__(self, max_threads, worker_func, worker_func_args_l,
                 final_callback=None, final_callback_args=None):
        super().__init__()
        self.worker_func = worker_func
        self.worker_func_args_l = worker_func_args_l
        self.max_threads = max_threads
        self.final_callback = final_callback
        self.final_callback_args = final_callback_args
        self.waiting_threads = []
        self.running_threads = []
        self.done_threads = []
        for args_tuple in self.worker_func_args_l:
            self.waiting_threads.append(
                threading.Thread(
                    group=None,
                    target=self._pool_worker,
                    name=None,
                    args=(*args_tuple,),
                    daemon=True
                )
            )

    def _pool_worker(self, *args):
        self.worker_func(*args)
        GLib.idle_add(self._rearrange_pool, threading.current_thread())

    def _rearrange_pool(self, t):
        self.running_threads.remove(t)
        self.done_threads.append(t)
        self.start()

    def is_alive(self):
        return not (
            len(self.running_threads) == 0
            and len(self.waiting_threads) == 0
        )

    def start(self):
        while (
                len(self.running_threads) < self.max_threads and
                len(self.waiting_threads) > 0
        ):
            t = self.waiting_threads.pop(0)
            t.start()
            self.running_threads.append(t)
        self.emit(
            'pool_progress',
            len(self.done_threads),
            len(self.worker_func_args_l)
        )
        if (
                len(self.running_threads) == 0
                and len(self.waiting_threads) == 0
        ):
            self.emit('pool_finished', '')
            if (
                    self.final_callback is not None
                    and self.final_callback_args is not None
            ):
                self.final_callback(*self.final_callback_args)
