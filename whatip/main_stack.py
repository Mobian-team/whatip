from gi.repository import Adw, GObject
from gettext import gettext as _
from whatip.network_interface_listbox import NetworkInterfaceListbox
from whatip.port_listbox import PortListbox
from whatip.lan_listbox import LanListbox
from whatip.network_interface import NetworkInterface


class MainStack(Adw.Bin):
    __gsignals__ = {
        'refresh_finished': (
            GObject.SignalFlags.RUN_LAST,
            None,
            (str,)
        )
    }

    def __init__(self):
        super().__init__(vexpand=True)
        self._stack = Adw.ViewStack(vexpand=True)
        self.set_child(self._stack)
        self.add_titled = self._stack.add_titled
        self.get_visible_child = self._stack.get_visible_child
        self.get_visible_child_name = self._stack.get_visible_child_name

        self.ip_listbox = NetworkInterfaceListbox()
        self.add_titled(self.ip_listbox, 'IP', _('IP')).set_icon_name(
            'network-wired-symbolic'
        )

        self.port_listbox = PortListbox()
        self.add_titled(self.port_listbox, 'Ports', _('Ports')).set_icon_name(
            'network-transmit-receive-symbolic'
        )

        self.lan_listbox = LanListbox()
        self.add_titled(self.lan_listbox, 'LAN', _('LAN')).set_icon_name(
            'preferences-system-network-symbolic'
        )
        self.lan_listbox.connect(
            'refresh_finished',
            self.on_lan_refresh_finished
        )

        self.show()

    def populate_lan_listbox(self):
        iface_l = list(filter(
            lambda iface: iface.interface_type not in (
                NetworkInterface.LOOPBACK,
                NetworkInterface.INTERNET,
                NetworkInterface.TUNNEL,
                NetworkInterface.UNKNOWN
            ),
            self.ip_listbox.iface_l
        ))
        if len(iface_l) <= 0:
            self.on_lan_refresh_finished()
            return
        self.lan_listbox.refresh([
            (iface.address, iface.subnet_mask)
            for iface in iface_l
        ])

    def on_lan_refresh_finished(self, *args):
        self.emit('refresh_finished', '')
