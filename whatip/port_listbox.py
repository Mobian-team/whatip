from gettext import gettext as _
from whatip.network_ports import get_ports
from whatip.port_listbox_row import PortListboxRow
from whatip.listbox_with_empty_state import ListBoxWithEmptyState


class PortListbox(ListBoxWithEmptyState):
    def __init__(self, *args, **kwargs):
        super().__init__(
            'network-offline-symbolic',
            _('No ports detected'),
            *args, **kwargs
        )

    def refresh(self):
        self.empty()
        ports = get_ports()
        for port in ports.keys():
            self.append(
                PortListboxRow(
                    '\n'.join(ports[port]['addresses']),
                    port,
                    ports[port]['type']
                )
            )
        if len(ports) <= 0:
            self.set_show_empty_state(True)
        else:
            self.show()
