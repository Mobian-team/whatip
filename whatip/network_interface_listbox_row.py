from gi.repository import Gtk
from whatip.network_interface import NetworkInterface
from gettext import gettext as _
from whatip.clipboard_helper import clipboard_copy


@Gtk.Template(resource_path='/org/gabmus/whatip/ui/net_iface_listbox_row.ui')
class NetworkInterfaceListboxRow(Gtk.ListBoxRow):
    __gtype_name__ = 'NetworkInterfaceListboxRow'
    mainbox = Gtk.Template.Child()
    icon = Gtk.Template.Child()
    address_label = Gtk.Template.Child()
    ipv6_box = Gtk.Template.Child()
    inet6_address_label = Gtk.Template.Child()
    interface_label = Gtk.Template.Child()
    location_label = Gtk.Template.Child()
    copy_btn = Gtk.Template.Child()
    copy_ipv6_btn = Gtk.Template.Child()

    NET_ICONS = {
        NetworkInterface.INTERNET: 'globe-symbolic',
        NetworkInterface.WIRED: 'network-wired-symbolic',
        NetworkInterface.WIRELESS: 'network-wireless-symbolic',
        NetworkInterface.MOBILE: 'phone-symbolic',
        NetworkInterface.VIRTUAL_BRIDGE: 'bridge-symbolic',
        NetworkInterface.TUNNEL: 'network-vpn-symbolic',
        NetworkInterface.LOOPBACK: 'media-playlist-repeat-symbolic',
        NetworkInterface.WIREGUARD: 'network-vpn-symbolic',
        NetworkInterface.UNKNOWN: 'dialog-question-symbolic'
    }
    NET_NAMES = {
        NetworkInterface.INTERNET: _('Public Internet'),
        NetworkInterface.WIRED: _('Wired'),
        NetworkInterface.WIRELESS: _('Wireless'),
        NetworkInterface.MOBILE: _('Mobile'),
        NetworkInterface.VIRTUAL_BRIDGE: _('Virtual bridge'),
        NetworkInterface.TUNNEL: _('Tunnel'),
        NetworkInterface.LOOPBACK: _('Loopback'),
        NetworkInterface.WIREGUARD: _('WireGuard'),
        NetworkInterface.UNKNOWN: _('Unknown')
    }

    def __init__(self, net_iface: NetworkInterface):
        super().__init__()
        self.net_iface = net_iface
        self.icon.set_from_icon_name(
            self.NET_ICONS[self.net_iface.interface_type]
        )
        self.icon.set_tooltip_text(self.NET_NAMES[
            self.net_iface.interface_type
        ])

        self.address_label.set_text(self.net_iface.address)

        if (hasattr(self.net_iface, 'inet6_address')):
            self.inet6_address_label.set_text(self.net_iface.inet6_address)
        else:
            self.ipv6_box.set_visible(False)

        self.interface_label.set_text(
            '{0}{1}'.format(
                self.NET_NAMES[self.net_iface.interface_type] + ': '
                if self.net_iface.interface_type != NetworkInterface.INTERNET
                else '',
                self.net_iface.interface
            )
        )

        if self.net_iface.location is not None:
            if self.net_iface.location['region'] is not None:
                self.location_label.set_text(_('{0}, {1} - {2}').format(
                    self.net_iface.location['city'],
                    self.net_iface.location['region'],
                    self.net_iface.location['country']
                ))
            else:
                self.location_label.set_text(_('{0} - {1}').format(
                    self.net_iface.location['city'],
                    self.net_iface.location['country']
                ))
            self.location_label.set_visible(True)
        else:
            self.location_label.set_visible(False)

        self.set_child(self.mainbox)

    @Gtk.Template.Callback()
    def on_copy_ipv4_clicked(self, *args):
        clipboard_copy(self.net_iface.address)

    @Gtk.Template.Callback()
    def on_copy_ipv6_clicked(self, *args):
        clipboard_copy(self.net_iface.inet6_address)
