from gi.repository import Gtk


class HidingListbox(Gtk.ListBox):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__hide()

    def __hide(self):
        self.set_visible(False)

    def __show(self):
        self.set_visible(True)
        self.show()

    def empty(self):
        self.__hide()
        while True:
            row = self.get_row_at_index(0)
            if row:
                super().remove(row)
            else:
                break

    def remove(self, *args, **kwargs):
        super().remove(*args, **kwargs)
        if len(self.get_children()) <= 0:
            self.__hide()

    def add(self, *args, **kwargs):
        super().add(*args, **kwargs)
        self.set_visible(True)
        self.show()
