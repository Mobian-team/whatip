import requests
import json
from gi.repository import Gtk, GLib
from gettext import gettext as _
from threading import Thread
from whatip.test_connection import is_online_sync


def __get_services():
    services = dict()
    lines = None
    with open('/etc/services', 'r') as fd:
        lines = fd.read().split('\n')
    for line in lines:
        if line == '' or line[0] == '#':
            continue
        line_split = line.split()
        port, port_type, service = (None, None, None)
        if len(line_split) > 2:
            if '#' in line:
                line = line.split('#')[0].strip()
            while '/' not in line.split()[-1] or line.count('/') > 1:
                line = ' '.join(line.split()[:-1])
            if len(line.split()) > 2:
                port, port_type = line.split()[-1].split('/')
                service = ' '.join(line.split()[:-1])
        if port is None:
            service = line_split[0]
            port, port_type = line_split[1].split('/')
        if port not in services.keys():
            services[port] = dict()
        services[port][port_type] = service
        if (
                'tcp' in services[port].keys()
                and 'udp' in services[port].keys()
                and services[port]['tcp'] == services[port]['udp']
        ):
            services[port]['tcp/udp'] = services[port]['tcp']
    return services


SERVICES = __get_services()


@Gtk.Template(resource_path='/org/gabmus/whatip/ui/net_port_listbox_row.ui')
class PortListboxRow(Gtk.ListBoxRow):
    __gtype_name__ = 'PortListboxRow'
    mainbox = Gtk.Template.Child()
    tcp_badge = Gtk.Template.Child()
    udp_badge = Gtk.Template.Child()
    port_label = Gtk.Template.Child()
    service_label = Gtk.Template.Child()
    address_label = Gtk.Template.Child()
    status_icon = Gtk.Template.Child()
    test_btn = Gtk.Template.Child()

    def __init__(self, address, port, port_type):
        super().__init__()
        self.port = port
        self.port_type = port_type
        self.address = address

        for t, b in zip(['TCP', 'UDP'], [self.tcp_badge, self.udp_badge]):
            if t in self.port_type:
                b.set_visible(True)
        self.port_label.set_text(self.port)
        self.service_label.set_text(
            SERVICES.get(
                self.port, {self.port_type.lower(): _('Unknown service')}
            ).get(self.port_type.lower(), _('Unknown service'))
        )
        self.address_label.set_text(self.address)

    def _test_port_async(self, retry=0):
        if not is_online_sync():
            GLib.idle_add(self.set_status, False)
        res = requests.get('https://ifconfig.co/port/'+self.port)
        reachable = False
        try:
            reachable = json.loads(res.text)['reachable']
        except Exception:
            print(_('Error parsing json'))
            if retry < 3:
                print(_('Retrying...'))
                return self._test_port_async(retry=retry+1)
        GLib.idle_add(
            self.set_status,
            reachable
        )

    def test_port(self):
        t = Thread(target=self._test_port_async)
        t.start()

    def set_status(self, reachable):
        self.status_icon.set_from_icon_name(
            'emblem-ok-symbolic' if reachable
            else 'window-close-symbolic'
        )
        self.status_icon.set_tooltip_text(
            _('Port reachable') if reachable
            else _('Port unreachable')
        )
        self.test_btn.set_sensitive(True)

    @Gtk.Template.Callback()
    def on_test_btn_clicked(self, btn):
        self.test_btn.set_sensitive(False)
        self.test_port()
