from gi.repository import Adw, GLib
from whatip.confManager import ConfManager
from whatip.headerbar import GHeaderbar
from whatip.main_stack import MainStack
from whatip.base_app import BaseWindow, AppShortcut


class AppWindow(BaseWindow):
    def __init__(self):
        super().__init__(
            app_name='What IP',
            icon_name='org.gabmus.whatip',
            shortcuts=[AppShortcut(
                'F10', lambda *args: self.headerbar.menu_btn.popup()
            )]
        )
        self.confman = ConfManager()

        self.main_stack = MainStack()
        self.bottom_bar = Adw.ViewSwitcherBar(
            stack=self.main_stack._stack, reveal=True
        )

        self.iface_listbox = self.main_stack.ip_listbox
        self.headerbar = GHeaderbar(self.main_stack)

        self.append(self.headerbar)
        self.append(self.main_stack)
        self.append(self.bottom_bar)

        self.headerbar.connect('headerbar_squeeze', self.on_headerbar_squeeze)

        self.confman.connect(
            'dark_mode_changed',
            lambda *args: self.set_dark_mode(self.confman.conf['dark_mode'])
        )
        self.set_dark_mode(self.confman.conf['dark_mode'])

    def present(self):
        super().present()
        self.set_default_size(
            self.confman.conf['windowsize']['width'],
            self.confman.conf['windowsize']['height']
        )
        GLib.timeout_add(
            100,
            lambda *args:
                self.bottom_bar.set_reveal(self.headerbar.is_squeezed())
        )

    def on_headerbar_squeeze(self, caller, squeezed):
        self.bottom_bar.set_reveal(squeezed)

    def emit_destroy(self, *args):
        self.emit('close-request')

    def on_destroy(self, *args):
        self.confman.conf['windowsize'] = {
            'width': self.get_width(),
            'height': self.get_height()
        }
        self.confman.save_conf()

    def do_startup(self):
        self.headerbar.on_refresh_btn_clicked()
