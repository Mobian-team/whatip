from gi.repository import Gtk, Gio, GLib
from whatip.clipboard_helper import clipboard_copy


@Gtk.Template(resource_path='/org/gabmus/whatip/ui/lan_listbox_row.ui')
class LanListboxRow(Gtk.ListBoxRow):
    __gtype_name__ = 'LanListboxRow'
    mainbox = Gtk.Template.Child()
    address_label = Gtk.Template.Child()
    hostname_label = Gtk.Template.Child()
    go_http_btn = Gtk.Template.Child()
    go_ftp_btn = Gtk.Template.Child()
    copy_btn = Gtk.Template.Child()

    def __init__(self, address, hostname, ports):
        super().__init__()
        self.address = address
        self.hostname = hostname
        self.address_label.set_text(self.address)
        self.hostname_label.set_text(self.hostname)

        port_interaction_buttons = {
            80: self.go_http_btn,
            21: self.go_ftp_btn
        }
        for port in port_interaction_buttons.keys():
            if port in ports:
                port_interaction_buttons[port].set_visible(True)

        self.set_child(self.mainbox)

    @Gtk.Template.Callback()
    def on_copy_btn_clicked(self, *args):
        clipboard_copy(self.address)

    @Gtk.Template.Callback()
    def on_go_http_btn_clicked(self, *args):
        Gio.AppInfo.launch_default_for_uri(
            f'http://{self.address}'
        )

    @Gtk.Template.Callback()
    def on_go_ftp_btn_clicked(self, *args):
        bus = Gio.bus_get_sync(Gio.BusType.SESSION, None)
        file_manager = Gio.DBusProxy.new_sync(
            bus,
            Gio.DBusProxyFlags.NONE,
            None,
            'org.freedesktop.FileManager1',
            '/org/freedesktop/FileManager1',
            'org.freedesktop.FileManager1',
            None
        )

        file_manager.call_sync(
            'ShowItems',
            GLib.Variant(
                '(ass)',
                ([f'ftp://{self.address}'], '')
            ),
            Gio.DBusCallFlags.NONE,
            -1,
            None
        )
