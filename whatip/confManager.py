from pathlib import Path
from os.path import isfile  # , isdir
# from os import makedirs
from os import environ as Env
import json
from gi.repository import GObject, Gio
from whatip.singleton import Singleton


class ConfManagerSignaler(GObject.Object):

    __gsignals__ = {
        'dark_mode_changed': (
            GObject.SignalFlags.RUN_FIRST,
            None,
            (str,)
        )
    }


class ConfManager(metaclass=Singleton):

    # _background_color = None

    BASE_SCHEMA = {
        'windowsize': {
            'width': 350,
            'height': 650
        },
        'dark_mode': False,
        'enable_third_parties': 'unset'
    }

    def __init__(self):
        self.window = None
        self.signaler = ConfManagerSignaler()
        self.emit = self.signaler.emit
        self.connect = self.signaler.connect

        # check if inside flatpak sandbox
        self.is_flatpak = (
            'XDG_RUNTIME_DIR' in Env.keys() and
            isfile(f'{Env["XDG_RUNTIME_DIR"]}/flatpak-info')
        )

        if self.is_flatpak:
            self.path = Path(
                f'{Env.get("XDG_CONFIG_HOME")}/org.gabmus.whatip.json'
            )
            self.cache_path = Path(
                f'{Env.get("XDG_CACHE_HOME")}/org.gabmus.whatip'
            )
        else:
            self.path = Path(
                f'{Env.get("HOME")}/.config/org.gabmus.whatip.json'
            )
            self.cache_path = Path(
                f'{Env.get("HOME")}/.cache/org.gabmus.whatip'
            )

        if self.path.is_file():
            try:
                with open(str(self.path)) as fd:
                    self.conf = json.loads(fd.read())
                # verify that the file has all of the schema keys
                for k in ConfManager.BASE_SCHEMA:
                    if k not in self.conf.keys():
                        if isinstance(
                                ConfManager.BASE_SCHEMA[k], (list, dict)
                        ):
                            self.conf[k] = ConfManager.BASE_SCHEMA[k].copy()
                        else:
                            self.conf[k] = ConfManager.BASE_SCHEMA[k]
            except Exception:
                self.conf = ConfManager.BASE_SCHEMA.copy()
                self.save_conf()
        else:
            self.conf = ConfManager.BASE_SCHEMA.copy()
            self.save_conf()

        # for p in [
        #         self.cache_path,
        # ]:
        #     if not isdir(p):
        #         makedirs(p)

        bl_gsettings = Gio.Settings.new('org.gnome.desktop.wm.preferences')
        bl = bl_gsettings.get_value('button-layout').get_string()
        self.wm_decoration_on_left = (
            'close:' in bl or
            'maximize:' in bl or
            'minimize:' in bl
        )

    def save_conf(self, *args):
        with open(str(self.path), 'w') as fd:
            fd.write(json.dumps(self.conf))

    # def get_background_color(self):
    #     if ConfManager._background_color is not None:
    #         return ConfManager._background_color
    #     if not self.window:
    #         return "000000"
    #     gc = self.window.get_style_context(
    #             ).get_background_color(Gtk.StateFlags.NORMAL)
    #     color = ''
    #     for channel in (gc.red, gc.green, gc.blue):
    #         color += '%02x' % int(channel*255)
    #     ConfManager._background_color = color
    #     return color
