from gi.repository import Gdk


def clipboard_copy(txt):
    Gdk.Display.get_default().get_clipboard().set(txt)
