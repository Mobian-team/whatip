from gi.repository import Gtk, GObject
from whatip.confManager import ConfManager


@Gtk.Template(resource_path='/org/gabmus/whatip/ui/headerbar.ui')
class GHeaderbar(Gtk.WindowHandle):
    __gtype_name__ = 'GHeaderbar'
    headerbar = Gtk.Template.Child()
    refresh_btn = Gtk.Template.Child()
    menu_btn = Gtk.Template.Child()
    squeezer = Gtk.Template.Child()
    nobox = Gtk.Template.Child()
    view_switcher = Gtk.Template.Child()

    __gsignals__ = {
        'headerbar_squeeze': (
            GObject.SignalFlags.RUN_FIRST,
            None,
            (bool,)
        )
    }

    def __init__(self, main_stack):
        super().__init__()
        self.main_stack = main_stack
        self.confman = ConfManager()
        self.view_switcher.set_stack(self.main_stack._stack)
        self.main_stack.connect('refresh_finished', self.on_refresh_finished)

    @Gtk.Template.Callback()
    def on_squeeze(self, *args):
        self.emit('headerbar_squeeze', self.is_squeezed())

    def is_squeezed(self) -> bool:
        return self.squeezer.get_visible_child() == self.nobox

    @Gtk.Template.Callback()
    def on_refresh_btn_clicked(self, *args, sync=False):
        self.refresh_btn.set_sensitive(False)
        self.main_stack.ip_listbox.refresh()
        self.main_stack.port_listbox.refresh()
        self.main_stack.populate_lan_listbox()

    def on_refresh_finished(self, *args):
        self.refresh_btn.set_sensitive(True)
